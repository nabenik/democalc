package com.nabenik.democalc;


import com.nabenik.democalc.Calculadora;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author tuxtor
 */
public class CalculadoraTest {
    
    Calculadora calculadora;
    public CalculadoraTest() {
    }
    
    @Before
    public void setUp() {
        calculadora = new Calculadora();
    }
    
    @Test
    public void probarSuma(){
        Assert.assertEquals(10, calculadora.sumar(5, 5));
    }
    
}
